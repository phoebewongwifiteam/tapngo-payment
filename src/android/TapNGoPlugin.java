package com.plugin.tapngoplugin;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

//Import for org.apache.cordova
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * This class echoes a string called from JavaScript.
 */
public class TapNGoPlugin extends CordovaPlugin {

    public static final String MER_TRADE_NO = "merTradeNo";
    public static final String MESSAGE = "message";
    public static final String TRADE_NO = "tradeNo";
    public static final String PAYMENT_CALLBACK_EVENT = "PAYMENT_CALLBACK_EVENT";
    public static final String PAYMENT_PARAMETERS = "com.plugin.tapngoplugin.PAYMENT_PARAMETERS";
    public static final String RECURRENT_PAYMENT_ACTION = "RECURRENT_PAYMENT_ACTION";
    public static final String RECURRENT_TOKEN = "recurrentToken";
    public static final String RESULT_CODE = "resultCode";
    public static final String SINGLE_AND_RECURRENT_PAYMENT_ACTION = "SINGLE_AND_RECURRENT_PAYMENT_ACTION";
    public static final String SINGLE_PAYMENT_ACTION = "SINGLE_PAYMENT_ACTION";

    public static final String TRADE_STATUS = "tradeStatus";

    private static final String ERR = "ERR";

    private CallbackContext mCallbackContext;

    private Context mContext;

    private LocalBroadcastReceiver mLocalBroadcastReceiver = new LocalBroadcastReceiver();

    class LocalBroadcastReceiver extends BroadcastReceiver {
        LocalBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            TapNGoPlugin.this.paymentResultCallback(intent.getStringExtra(TapNGoPlugin.RESULT_CODE), 
                intent.getStringExtra(TapNGoPlugin.RECURRENT_TOKEN), 
                intent.getStringExtra(TapNGoPlugin.MER_TRADE_NO), 
                intent.getStringExtra(TapNGoPlugin.TRADE_STATUS), 
                intent.getStringExtra(TapNGoPlugin.MESSAGE),
                intent.getStringExtra(TapNGoPlugin.TRADE_NO));
        }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext){
        mCallbackContext = callbackContext;
        mContext = this.cordova.getActivity().getApplicationContext();

        LocalBroadcastManager.getInstance(this.mContext).registerReceiver(this.mLocalBroadcastReceiver, new IntentFilter(PAYMENT_CALLBACK_EVENT));

        if (action.equals("getPluginVersion")) {
            getPluginVersion();
            return true;
        }else if (action.equals("doSinglePayment")) {
            doSinglePayment(args);
            return true;
        }else if (action.equals("doRecurrentPayment")) {
            doRecurrentPayment(args);
            return true;
        }else if (action.equals("doSingleAndRecurrentPayment")) {
            doSingleAndRecurrentPayment(args);
            return true;
        }
        return false;
    }

    private void doSinglePayment(JSONArray args) {
        try {
            String enableSandBox = args.getString(0);
            String APP_ID = args.getString(1);
            String API_KEY = args.getString(2);
            String PUBLIC_KEY = args.getString(3);
            String CALLBACK_ID = args.getString(4);
            String merTradeNo = args.getString(5);
            String totalPrice = args.getString(6);
            String currency = args.getString(7);
            String remark = args.getString(8);
            String notifyUrl = args.getString(9);

            Double mTotalPrice = Double.parseDouble(totalPrice);
            this.setSandBoxModeEnabled(enableSandBox);

            Intent paymentIntent = new Intent(this.mContext, TapNGoPluginPaymentActivity.class);
            TapNGoPluginPaymentParams params = new TapNGoPluginPaymentParams(APP_ID, API_KEY, PUBLIC_KEY, CALLBACK_ID, merTradeNo, mTotalPrice, currency, remark, notifyUrl);
            paymentIntent.setAction(SINGLE_PAYMENT_ACTION);
            paymentIntent.putExtra(PAYMENT_PARAMETERS, params);
            paymentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.mContext.startActivity(paymentIntent);


        }catch (Exception e){
            mCallbackContext.error(ERR);
        }

    }

    
    private void doRecurrentPayment(JSONArray args) {
        try {
            String enableSandBox = args.getString(0);
            String APP_ID = args.getString(1);
            String API_KEY = args.getString(2);
            String PUBLIC_KEY = args.getString(3);
            String CALLBACK_ID = args.getString(4);
            String currency = args.getString(5);
            String remark = args.getString(6);

            this.setSandBoxModeEnabled(enableSandBox);

            Intent paymentIntent = new Intent(this.mContext, TapNGoPluginPaymentActivity.class);
            TapNGoPluginPaymentParams params = new TapNGoPluginPaymentParams(APP_ID, API_KEY, PUBLIC_KEY, CALLBACK_ID, MER_TRADE_NO, null, currency, remark, null);
            paymentIntent.setAction(RECURRENT_PAYMENT_ACTION);
            paymentIntent.putExtra(PAYMENT_PARAMETERS, params);
            paymentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.mContext.startActivity(paymentIntent);

        }catch (Exception e){
            mCallbackContext.error(ERR);
        }

    }

    private void doSingleAndRecurrentPayment(JSONArray args) {
        try {
            String enableSandBox = args.getString(0);
            String APP_ID = args.getString(1);
            String API_KEY = args.getString(2);
            String PUBLIC_KEY = args.getString(3);
            String CALLBACK_ID = args.getString(4);
            String merTradeNo = args.getString(5);
            String totalPrice = args.getString(6);
            String currency = args.getString(7);
            String remark = args.getString(8);
            String notifyUrl = args.getString(9);

            Double mTotalPrice = Double.parseDouble(totalPrice);

            this.setSandBoxModeEnabled(enableSandBox);

            Intent paymentIntent = new Intent(this.mContext, TapNGoPluginPaymentActivity.class);
            TapNGoPluginPaymentParams params = new TapNGoPluginPaymentParams(APP_ID, API_KEY, PUBLIC_KEY, CALLBACK_ID, merTradeNo, mTotalPrice, currency, remark, notifyUrl);
            paymentIntent.setAction(SINGLE_AND_RECURRENT_PAYMENT_ACTION);
            paymentIntent.putExtra(PAYMENT_PARAMETERS, params);
            paymentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.mContext.startActivity(paymentIntent);
        }catch (Exception e){
            mCallbackContext.error(ERR);
        }
    }

    private void paymentResultCallback(String resultCode, String recurrentToken, String merTradeNo, String tradeStatus, String message, String tradeNo) {
        JSONObject result = new JSONObject();
        try {
            result.put(RESULT_CODE, resultCode);
            result.put(RECURRENT_TOKEN, recurrentToken);
            result.put(MER_TRADE_NO, merTradeNo);
            result.put(TRADE_STATUS, tradeStatus);
            result.put(TRADE_NO, tradeNo);
            result.put(MESSAGE, message);
            mCallbackContext.success(result);
        }catch (Exception e){
            mCallbackContext.error(ERR);
        }
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mLocalBroadcastReceiver);
    }

    private void setSandBoxModeEnabled(String enabled) {
        TapNGoPluginSettings.setSandboxMode(Boolean.parseBoolean(enabled));
    }

    private void getPluginVersion() {
        try {
            String pluginVersion = TapNGoPluginSettings.getPluginVersion();
            mCallbackContext.success(pluginVersion);
        }catch (Exception e) {
            mCallbackContext.error(ERR);
        }
    }

}


// public class TapNGoPlugin extends CordovaPlugin {

//     @Override
//     public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
//         if (action.equals("coolMethod")) {
//             String message = args.getString(0);
//             this.coolMethod(message, callbackContext);
//             return true;
//         }
//         return false;
//     }

//     private void coolMethod(String message, CallbackContext callbackContext) {
//         if (message != null && message.length() > 0) {
//             callbackContext.success(message);
//         } else {
//             callbackContext.error("Expected one non-empty string argument.");
//         }
//     }
// }
