#import "AppDelegate.h"

@interface AppDelegate (TGPlugin)

- (id) getCommandInstance:(NSString*)className;

- (void)createURLSchemeChecker:(NSNotification *)notification;

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

@end