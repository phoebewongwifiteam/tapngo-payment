//
//  TGPluginSettings.h
//  tapngoplugin

#import <Foundation/Foundation.h>

@interface TGPluginSettings : NSObject

+ (BOOL)isSandBoxModeEnabled;

+ (void)setSandBoxModeEnable:(BOOL)enable;

+ (NSString*)getpluginVersion;

@end
