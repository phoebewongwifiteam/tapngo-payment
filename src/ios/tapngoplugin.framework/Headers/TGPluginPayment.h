//
//  TapNGoPayment.h
//  tapngoplugin

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TGPluginPayment : NSObject

@property (strong, nonatomic) NSString* appId;
@property (strong, nonatomic) NSString* apiKey;
@property (strong, nonatomic) NSString* publicKey;
@property (strong, nonatomic) NSString* callBackId;

- (instancetype)initWithAppId:(NSString *)appId apiKey:(NSString *)apiKey publicKey:(NSString *)publicKey callBackId:(NSString *)callBackId;

- (void)setSinglePaymentWithMerTradeNo:(NSString*)merTradeNo totalPrice:(double)totalPrice currency:(NSString*)currency remark:(NSString*)remark notifyUrl:(NSString *)notifyUrl;

- (void)setRecurrentPaymentWithMerTradeNo:(NSString *)merTradeNo currency:(NSString*)currency remark:(NSString*)remark;

- (void)setSingleAndRecurrentPaymentWithMerTradeNo:(NSString*)merTradeNo totalPrice:(double)totalPrice currency:(NSString*)currency remark:(NSString*)remark notifyUrl:(NSString *)notifyUrl;

@end
