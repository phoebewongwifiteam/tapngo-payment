//
//  PayResult.h
//  tapngoplugin


#import <Foundation/Foundation.h>

typedef enum {
    TGPluginPayResultTradeStatusTradeFinished,
    TGPluginPayResultTradeStatusTradeClosed,
    TGPluginPayResultTradeStatusTradeWaitToPay,
    TGPluginPayResultTradeStatusUnknown
} TGPluginPayResultTradeStatus;

@interface TGPluginPayResult : NSObject

@property (strong, nonatomic) NSString* resultCode;
@property (strong, nonatomic) NSString* recurrentToken;
@property (strong, nonatomic) NSString* merTradeNo;
@property (strong, nonatomic) NSString* tradeNo;
@property (assign, nonatomic) TGPluginPayResultTradeStatus tradeStatus;
@property (strong, nonatomic) NSString* message;

- (instancetype)initItemWithURL:(NSURL *)URL;
- (instancetype)initFailResultWithResultCode:(NSString *)resultCode merTradeNo:(NSString *)merTradeNo msg:(NSString *)msg;
- (instancetype)initPluginFailResultWithResultCode:(NSString *)resultCode merTradeNo:(NSString *)merTradeNo;

- (NSString *)getStringForTradeStatus:(TGPluginPayResultTradeStatus)tradeStatus;

- (NSString*) getResultCode;
- (NSString*) getRecurrentToken;
- (NSString*) getMerTradeNo;
- (NSString*) getTradeNo;
- (TGPluginPayResultTradeStatus) getTradeStatus;
- (NSString*) getMessage;

@end
