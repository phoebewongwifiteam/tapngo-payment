//
//  TGPluginAppDelegate.h
//  tapngoplugin


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TGPluginAppDelegate : NSObject

+ (instancetype)sharedInstance;

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end