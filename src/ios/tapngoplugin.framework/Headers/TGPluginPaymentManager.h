//
//  TapNGoPaymentManager.h
//  tapngoplugin

/**
 *  Manager to trigger payment
 *
 */

#import <Foundation/Foundation.h>
#import "TGPluginPaymentResultDelegate.h"
#import "TGPluginPayment.h"

@interface TGPluginPaymentManager : NSObject

@property (strong, nonatomic) id<TGPluginPaymentResultDelegate> delegate;

/**
 *  Initializer
 *
 *  @param delegate Class which implemented TapNGoPaymentResult Delegate
 *
 *  @return TapNGoPaymentManager instance
 */
- (instancetype)initWithDelegate:(id<TGPluginPaymentResultDelegate>)delegate;


/**
 *  Do payment
 *
 *  @param payment Instance of TapNGoPayment
 */
- (void)doPayment:(TGPluginPayment*)payment;

@end
