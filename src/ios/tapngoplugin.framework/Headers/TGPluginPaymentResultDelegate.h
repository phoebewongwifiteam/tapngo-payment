//
//  TapNGoPaymentResultDelegate.h
//  tapngplugin


#import <Foundation/Foundation.h>
#import "TGPluginPayResult.h"

@protocol TGPluginPaymentResultDelegate <NSObject>

@required

/**
 *  Call back protocol for success case
 *
 *  @param payResult Object to store payment result
 */
- (void) doPaymentSuccessWithPayResult:(TGPluginPayResult*)payResult;


/**
 *  Call back protocol for fail case
 *
 *  @param payResult Object to store payment result
 */
- (void) doPaymentFailWithPayResult:(TGPluginPayResult*)payResult;

@end
