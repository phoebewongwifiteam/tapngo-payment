/********* TapNGoPlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <tapngoplugin/TGPluginSettings.h>
#import <tapngoplugin/TGPluginPaymentResultDelegate.h>
#import <tapngoplugin/TGPluginPaymentManager.h>

@interface TapNGoPlugin : CDVPlugin <TGPluginPaymentResultDelegate>{
  // Member variables go here.
}

- (void)getPluginVersion:(CDVInvokedUrlCommand*)command;
- (void)doSinglePayment:(CDVInvokedUrlCommand*)command;
- (void)doRecurrentPayment:(CDVInvokedUrlCommand*)command;
- (void)doSingleAndRecurrentPayment:(CDVInvokedUrlCommand*)command;
- (void)setSandBoxModeEnable:(BOOL)enable;


// - (void)coolMethod:(CDVInvokedUrlCommand*)command;
@end

@implementation TapNGoPlugin {
    NSString *callbackId;
    TGPluginPaymentManager *manager;
}

-(void)printDebugLog:(NSString *)appId apiKey:(NSString*)apiKey publicKey:(NSString*)publicKey callbackId:(NSString*)callbackId merTradeNo:(NSString*)merTradeNo price:(NSString*)price currency:(NSString*)currency remark:(NSString*)remark notifyUrl:(NSString*)notifyUrl {
#ifdef DEBUG
    NSLog(@"appId: %@", appId);
    NSLog(@"apiKey: %@", apiKey);
    NSLog(@"publicKey: %@", publicKey);
    NSLog(@"callbackId: %@", callbackId);
    NSLog(@"merTradeNo: %@", merTradeNo);
    NSLog(@"price String: %@", price);
    NSLog(@"price doubleValue: %f", [price doubleValue]);
    NSLog(@"currency: %@", currency);
    NSLog(@"remark: %@", remark);
    NSLog(@"notifyUrl: %@", notifyUrl);
#endif
}

-(void)printDebugLog:(NSString*)functionName payResult:(TGPluginPayResult *)payResult {
#ifdef DEBUG
    NSLog(@"resultCode: %@", payResult.resultCode);
    NSLog(@"recurrentToken: %@" , payResult.recurrentToken);
    NSLog(@"merTradeNo: %@" , payResult.merTradeNo);
    NSLog(@"tradeStatus: %@" , [payResult getStringForTradeStatus:payResult.tradeState]);
    NSLog(@"tradeNo: %@", payResult.tradeNo);
    NSLog(@"message: %@", payResult.message);
#endif
}

- (void)doSinglePayment:(CDVInvokedUrlCommand*)command
{
    callbackId = command.callbackId;
    
    NSString *sandBoxEnable = [command.arguments objectAtIndex:0];
    NSString *appId = [command.arguments objectAtIndex:1];
    NSString *apiKey = [command.arguments objectAtIndex:2];
    NSString *publicKey = [command.arguments objectAtIndex:3];
    NSString *callbackId = [command.arguments objectAtIndex:4];
    NSString *merTradeNo = [command.arguments objectAtIndex:5];
    NSString *price = [command.arguments objectAtIndex:6];
    NSString *currency = [command.arguments objectAtIndex:7];
    NSString *remark = [command.arguments objectAtIndex:8];
    NSString *notifyUrl = [command.arguments objectAtIndex:9];
    
    [self printDebugLog:appId apiKey:apiKey publicKey:publicKey callbackId:callbackId merTradeNo:merTradeNo price:price currency:currency remark:remark notifyUrl:notifyUrl];
    
    [self setSandBoxModeEnable:[sandBoxEnable boolValue]];
    
    double _price = [price doubleValue];
    
    TGPluginPayment *payment = [[TGPluginPayment alloc]initWithAppId:appId apiKey:apiKey publicKey:publicKey callBackId:callbackId];
    
    [payment setSinglePaymentWithMerTradeNo:merTradeNo totalPrice:_price currency:currency remark:remark notifyUrl:notifyUrl];
    
    if (NULL == manager) {
        manager = [[TGPluginPaymentManager alloc] initWithDelegate:self];
    }
    
    [manager doPayment:payment];

}

- (void)doRecurrentPayment:(CDVInvokedUrlCommand*)command
{
    callbackId = command.callbackId;
    
    NSString *sandBoxEnable = [command.arguments objectAtIndex:0];
    NSString *appId = [command.arguments objectAtIndex:1];
    NSString *apiKey = [command.arguments objectAtIndex:2];
    NSString *publicKey = [command.arguments objectAtIndex:3];
    NSString *callbackId = [command.arguments objectAtIndex:4];
    NSString *merTradeNo = [command.arguments objectAtIndex:5];
    NSString *currency = [command.arguments objectAtIndex:6];
    NSString *remark = [command.arguments objectAtIndex:7];
    
    [self printDebugLog:appId apiKey:apiKey publicKey:publicKey callbackId:callbackId merTradeNo:merTradeNo price:NULL currency:currency remark:remark notifyUrl:NULL];
    
    [self setSandBoxModeEnable:[sandBoxEnable boolValue]];
    
    TGPluginPayment *payment = [[TGPluginPayment alloc]initWithAppId:appId apiKey:apiKey publicKey:publicKey callBackId:callbackId];
    
    [payment setRecurrentPaymentWithMerTradeNo:merTradeNo currency:currency remark:remark];
    
    if (NULL == manager) {
        manager = [[TGPluginPaymentManager alloc] initWithDelegate:self];
    }
    
    [manager doPayment:payment];
}

- (void)doSingleAndRecurrentPayment:(CDVInvokedUrlCommand*)command
{
    callbackId = command.callbackId;
    
    NSString *sandBoxEnable = [command.arguments objectAtIndex:0];
    NSString *appId = [command.arguments objectAtIndex:1];
    NSString *apiKey = [command.arguments objectAtIndex:2];
    NSString *publicKey = [command.arguments objectAtIndex:3];
    NSString *callbackId = [command.arguments objectAtIndex:4];
    NSString *merTradeNo = [command.arguments objectAtIndex:5];
    NSString *price = [command.arguments objectAtIndex:6];
    NSString *currency = [command.arguments objectAtIndex:7];
    NSString *remark = [command.arguments objectAtIndex:8];
    NSString *notifyUrl = [command.arguments objectAtIndex:9];
    
    [self printDebugLog:appId apiKey:apiKey publicKey:publicKey callbackId:callbackId merTradeNo:merTradeNo price:price currency:currency remark:remark notifyUrl:notifyUrl];
    
    [self setSandBoxModeEnable:[sandBoxEnable boolValue]];
    
    double _price = [price doubleValue];
    
    TGPluginPayment *payment = [[TGPluginPayment alloc]initWithAppId:appId apiKey:apiKey publicKey:publicKey callBackId:callbackId];
    
    [payment setSingleAndRecurrentPaymentWithMerTradeNo:merTradeNo totalPrice:_price currency:currency remark:remark notifyUrl:notifyUrl];
    
    if (NULL == manager) {
        manager = [[TGPluginPaymentManager alloc] initWithDelegate:self];
    }
    
    [manager doPayment:payment];
    
}

- (void)getPluginVersion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    callbackId = command.callbackId;
    
    NSString *pluginVersion = [TGPluginSettings getPluginVersion];
    
    if (NULL != pluginVersion && pluginVersion.length > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:pluginVersion];
    }else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString: @"ERR"];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

-(void)sendPayResultEvent:(NSString *)_subscriptionName isSuccessful:(BOOL)isSuccessful payResult:(TGPluginPayResult *)payResult {
    
    NSString *resultCode = NULL == payResult.resultCode ? @"" : payResult.resultCode;
    NSString *recurrentToken = NULL == payResult.recurrentToken ? @"" : payResult.recurrentToken;
    NSString *merTradeNo = NULL == payResult.merTradeNo ? @"" : payResult.merTradeNo;
    NSString *tradeStatus = [payResult getStringForTradeStatus:payResult.tradeStatus];
    NSString *tradeNo = NULL == payResult.tradeNo ? @"" : payResult.tradeNo;
    NSString *message = NULL == payResult.message ? @"" : payResult.message;
    
    CDVPluginResult* pluginResult = nil;
    
    CDVCommandStatus resultStatus = isSuccessful ? CDVCommandStatus_OK : CDVCommandStatus_ERROR;
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: resultCode, @"resultCode",
                                                                     recurrentToken, @"recurrentToken",
                                                                     merTradeNo, @"merTradeNo",
                                                                     tradeStatus, @"tradeStatus",
                                                                     tradeNo, @"tradeNo",
                                                                     message, @"message",
                                                                     nil];
    
    pluginResult = [CDVPluginResult resultWithStatus:resultStatus messageAsDictionary:dict];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void)setSandBoxModeEnable:(BOOL)enable
{
    [TGPluginSettings setSandBoxModeEnable:enable];
}

#pragma mark Tap&Go Plugin delegate
- (void)doPaymentFailWithPayResult:(TGPluginPayResult *)payResult {
    
    [self printDebugLog:@"doPaymentFailWithPayResult" payResult:payResult];
    
    [self sendPayResultEvent:callbackId isSuccessful:false payResult:payResult];
}

- (void)doPaymentSuccessWithPayResult:(TGPluginPayResult *)payResult {
    
    [self printDebugLog:@"doPaymentSuccessWithPayResult" payResult:payResult];
    
    [self sendPayResultEvent:callbackId isSuccessful:true payResult:payResult];
}

- (void)doPaymentErrorWithPayResult:(TGPluginPayResult*)payResult {
    
    [self printDebugLog:@"doPaymentErrorWithPayResult" payResult:payResult];
    
    [self sendPayResultEvent:callbackId isSuccessful:false payResult:payResult];
}


// - (void)coolMethod:(CDVInvokedUrlCommand*)command
// {
//     CDVPluginResult* pluginResult = nil;
//     NSString* echo = [command.arguments objectAtIndex:0];

//     if (echo != nil && [echo length] > 0) {
//         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
//     } else {
//         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//     }

//     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
// }

@end
