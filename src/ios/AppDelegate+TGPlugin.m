#import "AppDelegate+TGPlugin.h"
#import <objc/runtime.h>
#import <tapngoplugin/TGPluginAppDelegate.h>

@implementation AppDelegate(TGPlugin)

- (id) getCommandInstance:(NSString*)className
{
	return [self.viewController getCommandInstance:className];
}

// its dangerous to override a method from within a category.
// Instead we will use method swizzling. we set this up in the load call.
+ (void)load
{
    Method original, swizzled;
    
    original = class_getInstanceMethod(self, @selector(init));
    swizzled = class_getInstanceMethod(self, @selector(swizzled_init));
    method_exchangeImplementations(original, swizzled);
}

- (AppDelegate *)swizzled_init
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createURLSchemeChecker:)
               name:@"UIApplicationDidFinishLaunchingNotification" object:nil];
	
	// This actually calls the original init method over in AppDelegate. Equivilent to calling super
	// on an overrided method, this is not recursive, although it appears that way. neat huh?
	return [self swizzled_init];
}

- (void)createURLSchemeChecker:(NSNotification *)notification
{
	NSLog(@"createURLSchemeChecker");
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
  [[TGPluginAppDelegate sharedInstance] application:application handleOpenURL:url];
  return YES;
}
  
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
  [[TGPluginAppDelegate sharedInstance] application:app handleOpenURL:url];
  return YES;
}

@end