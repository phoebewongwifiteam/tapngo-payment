var exec = require('cordova/exec');

// exports.coolMethod = function (arg0, success, error) {
//     exec(success, error, 'TapNGoPlugin', 'coolMethod', [arg0]);
// };

exports.getPluginVersion = function (success, error) {
	if (error == null) { errorCallback = function() {}}

    if (typeof success != "function")  {
        console.log("getPluginVersion failure: failure parameter not a function");
        return
    }

    if (typeof error != "function") {
        console.log("getPluginVersion failure: success callback parameter must be a function");
        return
	}

    exec(success, error, 'TapNGoPlugin', 'getPluginVersion');
};


exports.doSinglePayment = function (sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, price, currency, remark, notifyUrl, success, error) {
	if (error == null) { errorCallback = function() {}}

    if (typeof error != "function")  {
        console.log("doSinglePayment failure: failure parameter not a function");
        return
    }

    if (typeof success != "function") {
        console.log("doSinglePayment failure: success callback parameter must be a function");
        return
	}
    exec(success, error, 'TapNGoPlugin', 'doSinglePayment', [sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, price, currency, remark, notifyUrl]);
};

exports.doRecurrentPayment = function (sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, currency, remark, success, error) {
	if (error == null) { errorCallback = function() {}}

    if (typeof error != "function")  {
        console.log("doRecurrentPayment failure: failure parameter not a function");
        return
    }

    if (typeof success != "function") {
        console.log("doRecurrentPayment failure: success callback parameter must be a function");
        return
	}
    exec(success, error, 'TapNGoPlugin', 'doRecurrentPayment', [sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, currency, remark]);
};

exports.doSingleAndRecurrentPayment = function (sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, price, currency, remark, notifyUrl, success, error) {
	if (error == null) { errorCallback = function() {}}

    if (typeof error != "function")  {
        console.log("doSingleAndRecurrentPayment failure: failure parameter not a function");
        return
    }

    if (typeof success != "function") {
        console.log("doSingleAndRecurrentPayment failure: success callback parameter must be a function");
        return
	}
    exec(success, error, 'TapNGoPlugin', 'doSingleAndRecurrentPayment', [sandBox, appId, apiKey, publicKey, callbackId, merTradeNo, price, currency, remark, notifyUrl]);
};



